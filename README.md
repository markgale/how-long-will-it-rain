# How long will it rain?

Quaranting hack project - [launch here](https://markgale.gitlab.io/how-long-will-it-rain/)

![MapBox logo](/docs/mapbox-logo.png "MapBox")  ![OpenWeather logo](/docs/openweather-logo.png "OpenWeather")

A quick and dirty concept demostrator pulling together [MapBox](https://www.mapbox.com/) and [OpenWeather](https://openweathermap.org/) APIs to answer the simple question of:

What does the rain (precipitation) and cloud look like where I am NOW and for the next couple of hours?

## Screenshot

![Screenshot](/docs/screenshot.png "Screenshot")




